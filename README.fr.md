# Microbe MVC

Microbe MVC est un **microframework PHP**, implémentant une architecture MVC (Model View Controller), le tout en POO (Programmation Orientée Objet).

Son code source tient dans à peu près **80 lignes de code PHP** !

Avant de commencer à étudier son fonctionnement, des bases préalables en PHP, MySQL et POO sont indispensables.

Ce microframework est un projet à **vocation éducative**, il convient uniquement pour des besoins d'**apprentissage**. Sa vocation première est de présenter aux programmeurs débutants un **cadre volontairement simple**, leur permettant de lire et comprendre comment MVC et le routage fonctionnent ensemble.

Lisez ceci dans d'autres langues: [Anglais](README.md), [Français](README.fr.md)

## Auteurs

- [Frederic Poeydomenge](https://gitlab.com/dyno64100)

## Description de l'arborescence

```
microbe             Microframework Microbe MVC
    bootstrap.php       Système d'amorçage de l'application
    controller.php      Controleur de base
    model.php           Modèle de base
    view.php            Vue de base

sample              Application de test utilisant le microframework
    controller/         Les controleurs de l'application
    model/              Les modèles de l'application
    view/               Les vues de l'application
    index.php           Point d'entrée de l'application
    .htaccess           Règles de réécritures Apache (cf routage)

apache2.conf        Fichier de configuration Apache
create_mysql.sql    Script d'initialisation de la base de données (version MySQL)
create_sqlite.sql   Script d'initialisation de la base de données (version SQLite)
```

## Téléchargement

```
git clone https://gitlab.com/dyno64100/microbe-mvc.git /var/www
```

## Installation

### 1. Création d'un nom de domaine local

Sous **Linux**, ajouter dans le fichier **/etc/hosts** :

    127.0.0.1 microbe-mvc.local

Sous **Windows**, le fichier hosts peut se trouver à l'emplacement suivant : `%SystemRoot%\system32\drivers\etc\hosts`

### 2. Création de l'hôte virtuel Apache

Un exemple de configuration d'un virtual host se trouve dans le fichier **apache2.conf**

Il faut le copier dans le dossier `sites-enabled` de Apache (_la procédure dépend de votre installation locale LAMP/XAMPP/WAMP/EasyPHP/..._), puis le paramétrer selon vos besoins.

On y configure **le nom de domaine local**, défini précédemment, qui permettra d'accéder au site via une adresse locale :

    ServerName microbe-mvc.local

ainsi que **le dossier** dans lequel l'arborescence décrite précédemment a été copiée :

    DocumentRoot /var/www/microbe-mvc/sample/
    <Directory /var/www/microbe-mvc/sample/>

Sous **Windows**, ces 2 directives ressembleraient plutôt à quelque chose du genre :

    DocumentRoot "D:/www/microbe-mvc/sample/"
    <Directory "D:/www/microbe-mvc/sample/">

**Attention!** Il faut veiller à bien faire pointer l'hôte virtuel sur le dossier `sample` de l'arborescence de Microbe MVC. C'est celui dans lequel se trouve le point d'entrée `index.php`.

### 3. Création de la base de données

Pour stocker les données de test, vous pouvez choisir d'utiliser comme backend soit une base de données MySQL, soit une base SQLite.

Selon votre choix de base de donnée, il faudra aller modifier le connecteur PDO, dans le script `sample/index.php` (constantes `BDD_*`), et commenter/décommenter le bloc de code correspondant (cf "Define database-related constants")

#### 3.a MySQL

Le script d'initialisation de la base de données se trouve dans le fichier **create_mysql.sql**

Il faut le lancer en tant que root/administrateur dans l'instance MySQL installée sur la machine

Il effectue les opérations suivantes :
- Création de la base de données `microbe_mvc`
- Création de l'utilisateur `user_mvc` avec les droits sur la base de données
- Création et remplissage de la table de test

#### 3.b SQLite3

A l'aide du DB Browser pour SQLite, il faut au préalable créer une nouvelle base de données à la racine du site, nommée `sqlite.db`.

Ensuite il suffit d'importer le fichier **create_sqlite.sql** (menu Fichier > Importer) pour créer et remplir le la table de test

### 4. Naviguer sur le site Web

Pointer votre navigateur sur le nom de domaine local (http://microbe-mvc.local/)

La page d'accueil devrait maintenant apparaitre !

Et maintenant, à vous de jouer !!!

## Fonctionnalités alternatives

D'autres fonctionnalités sont disponibles sur les branches GIT suivantes :

- [feature/namespace](https://gitlab.com/dyno64100/microbe-mvc/-/tree/feature/namespace) : exemple d'utilisation des espaces de noms PHP
- [feature/phpunit](https://gitlab.com/dyno64100/microbe-mvc/-/tree/feature/phpunit) : exemple d'exécution de tests unitaires

Pour mettre à jour le code, afin de tester une branche ci-dessus, effectuer un :

```
git checkout feature/<Fonctionnalité>
```

## Annexe

### Routage des URL

Le routage est responsable de la correspondance des requêtes HTTP entrantes (_cad. l'adresse saisie dans le navigateur_) et de la distribution de ces requêtes aux points de terminaison exécutables de l’application (_cad. la méthode du contrôleur à appeler_)

Dans Microbe MVC, il n'est pas nécessaire de définir les routes manuellement, le routage est automatique, et achemine la requête HTTP en fonction des différents composants de l'URL : par exemple, pour la requête http://nomdedomaine/recipe/detail/12, le système va appeler le controlleur **Recipe**, méthode **detail()**, en lui passant en paramètre la valeur **12**

Pour ce faire, on utilise deux mécanismes distincts :

- le **module Apache mod_rewrite**, qui permet de mettre en correspondance l'URL avec le système de fichiers. Les instructions détaillées dans le fichier de configuration `sample/.htaccess` indiquent à Apache que toutes les demandes ne correspondant pas physiquement à un fichier sont routées vers le fichier `sample/index.php`.
- l'**auto-chargement de classes (autoload)**, défini dans le script `microbe/bootstrap.php` qui est chargé au démarrage de la page, et qui permet de mapper le nom de la classe demandée avec un fichier physique dans l'arborescence.

## Licence

Ce projet est distribué sous Licence Expat (#Expat) connue aussi sous le nom de « Licence MIT »
[Voir le texte intégral de la Licence MIT (#Expat)](LICENCE.fr.md)

Le logo "Microbe" est distributé sous licence Attribution 3.0 Unported (CC BY 3.0)
[Télécharger l'icone sur Iconfinder](https://www.iconfinder.com/icons/6009592)
