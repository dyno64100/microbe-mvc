<?php

/**
 * Microbe MVC: Sample usage - Entry point
 *
 * @copyright 2022 Frederic Poeydomenge <dyno@aldabase.com>
 * @license https://opensource.org/licenses/MIT MIT Licence (#Expat)
 */

// Define constants related to the location of the site
define('SITE_URL', '//' . $_SERVER['HTTP_HOST']);
define('SITE_PATH', __DIR__);
define('SITE_VERSION', '<b>Standard</b> version');

// Define database-related constants (MySQL version)
define('BDD_DSN', 'mysql:host=localhost;dbname=microbe_mvc;charset=utf8mb4');
define('BDD_USER', 'user_mvc');
define('BDD_PASSWORD', 'pass_mvc');
// Define database-related constants (SQLite version)
// define('BDD_DSN', 'sqlite:' . __DIR__ . '/../sqlite.db');
// define('BDD_USER', null);
// define('BDD_PASSWORD', null);

// Microbe MVC Configuration
define('MICROBE_PREFIX', 'SampleController');
define('MICROBE_CONTROL', 'homepage');
define('MICROBE_VIEW_DIR', __DIR__ . '/view/');

// Launches the application bootstrapping system
require_once __DIR__ . '/../microbe/bootstrap.php';
