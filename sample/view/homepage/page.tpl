<div class="px-4 py-4 my-4 text-center">
  <h1 class="display-5 fw-bold">
    Microbe MVC
  </h1>
  <div class="col-lg-6 mx-auto">
    <p class="lead">
      Welcome on the microframework default page.<br>
    </p>
  </div>
</div>
<p class="text-center">
  <i class="bi bi-award" aria-hidden="true"></i>
  Last recipe added to the site :
  <a href="<?= SITE_URL ?>/recipe/detail/<?= $last['id'] ?>">
    <?= htmlentities($last['name']) ?>
  </a>
</p>