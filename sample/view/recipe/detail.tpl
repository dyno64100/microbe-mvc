<h1>
  <?= htmlentities($recipe['name']) ?>
  <a href="<?= SITE_URL ?>/recipe/form/<?= $recipe['id'] ?>" class="text-primary">
    <i class="bi bi-pencil-fill" aria-hidden="true"></i>
  </a>
</h1>

<div class="row">
  <div class="col-4">
    <dl>
      <dt>Type</dt>
      <dd><?= htmlentities($recipe['type']) ?></dd>
      <dt>Difficulty</dt>
      <dd><?= htmlentities($recipe['difficulty']) ?></dd>
      <dt>Cost</dt>
      <dd><?= htmlentities($recipe['cost']) ?></dd>
      <dt>Servings</dt>
      <dd><?= htmlentities($recipe['servings']) ?></dd>
    </dl>
  </div>
  <div class="col-8">
    <img src="https://picsum.photos/seed/<?= $recipe['id'] ?>/1200/300?grayscale" class="img-fluid img-thumbnail" alt="">
  </div>
</div>
<hr>
<div class="row">
  <div class="col-4">
    <h2>Ingredients</h2>
    <ul>
      <li>Lorem ipsum</li>
      <li>Dolor sit amet</li>
      <li>Consectetur</li>
      <li>Adipiscing elit</li>
    </ul>
  </div>
  <div class="col-8">
    <h2>Instructions</h2>
    <ul>
      <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
      <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
      <li>Curabitur gravida arcu ac tortor dignissim convallis aenean et.</li>
      <li>Ornare lectus sit amet est placerat in egestas erat imperdiet.</li>
      <li>Sit amet est placerat in egestas erat.</li>
    </ul>
  </div>
</div>