<?php $isUpdate = $recipe !== null; ?>

<h1><?= $isUpdate ? 'Update the recipe' : 'Add a recipe' ?></h1>

<form action="/recipe/form" method="post">

  <input type="hidden" name="id" value="<?= $isUpdate ? $recipe['id'] : '' ?>">

  <div class="mb-3">
    <label for="recipeName" class="form-label"> Name <span class="text-danger">*</span> </label>
    <input type="text" class="form-control" id="recipeName" name="name" placeholder="Enter value" value="<?= $isUpdate ? htmlentities($recipe['name']) : '' ?>" required>
  </div>

  <div class="mb-3">
    <label for="recipeType" class="form-label"> Type <span class="text-danger">*</span> </label>
    <select id="recipeType" name="type" class="form-select" required>
      <option value="">Choose</option>
      <?php foreach ($providerType as $value) { ?>
      <option value="<?= $value ?>" <?=$isUpdate && $recipe['type']===$value ? 'selected' : '' ?>><?= $value ?></option>
      <?php } ?>
    </select>
  </div>

  <div class="mb-3">
    <label for="recipeDifficulty" class="form-label"> Difficulty <span class="text-danger">*</span> </label>
    <select id="recipeDifficulty" name="difficulty" class="form-select" required>
      <option value="">Choose</option>
      <?php foreach ($providerDifficulty as $value) { ?>
      <option value="<?= $value ?>" <?=$isUpdate && $recipe['difficulty']===$value ? 'selected' : '' ?>><?= $value ?></option>
      <?php } ?>
    </select>
  </div>

  <div class="mb-3">
    <label for="recipeCost" class="form-label"> Cost <span class="text-danger">*</span> </label>
    <select id="recipeCost" name="cost" class="form-select" required>
      <option value="">Choose</option>
      <?php foreach ($providerCost as $value) { ?>
      <option value="<?= $value ?>" <?=$isUpdate && $recipe['cost']===$value ? 'selected' : '' ?>><?= $value ?></option>
      <?php } ?>
    </select>
  </div>

  <div class="mb-3">
    <label for="recipeServings" class="form-label"> Servings <span class="text-danger">*</span> </label>
    <input type="number" class="form-control" id="recipeServings" name="servings" placeholder="Enter value" value="<?= $isUpdate ? htmlentities($recipe['servings']) : '' ?>" required>
  </div>

  <button class="btn btn-primary" type="submit"><?= $isUpdate ? 'Update the recipe' : 'Add the recipe' ?></button>

</form>