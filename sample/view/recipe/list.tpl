<h1>List of recipes</h1>

<?php if (! empty($_SESSION['message'])) { ?>
<div class="alert alert-success">
  <i class="bi bi-check-circle-fill pe-2" aria-hidden="true"></i>
  <strong><?= $_SESSION['message'] ?></strong>
</div>
<?php unset($_SESSION['message']); } ?>

<table class="table table-bordered table-striped" aria-describedby="List of recipes">
  <thead>
    <tr class="table-dark text-center">
      <th scope="col">Name</th>
      <th scope="col">Type</th>
      <th scope="col">Difficulty</th>
      <th scope="col">Cost</th>
      <th scope="col">Servings</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($recipes as $recipe): ?>
    <tr>
      <td>
        <a href="<?= SITE_URL ?>/recipe/detail/<?= $recipe['id'] ?>">
          <?= htmlentities($recipe['name']) ?>
        </a>
      </td>
      <td class="text-center"><?= htmlentities($recipe['type']) ?></td>
      <td class="text-center"><?= htmlentities($recipe['difficulty']) ?></td>
      <td class="text-center"><?= htmlentities($recipe['cost']) ?></td>
      <td class="text-center"><?= htmlentities($recipe['servings']) ?></td>
      <td class="text-center">
        <a href="<?= SITE_URL ?>/recipe/form/<?= $recipe['id'] ?>" class="text-primary me-2">
          <i class="bi bi-pencil-fill" aria-hidden="true"></i>
        </a>
        <a href="<?= SITE_URL ?>/recipe/delete/<?= $recipe['id'] ?>" class="text-danger">
          <i class="bi bi-trash-fill" aria-hidden="true"></i>
        </a>
      </td>
    </tr>
    <?php endforeach ?>
  </tbody>
</table>