<!doctype html>
<html lang="fr">

  <!-- HEAD -->
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Microbe MVC</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="<?= SITE_URL ?>/index.css">
    <link rel="icon" type="image/png" href="<?= SITE_URL ?>/favicon.png">
  </head>

  <!-- BODY -->
  <body>
    <div class="container">
      <!-- NAVIGATION BAR -->
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
          <a class="navbar-brand" href="/">
            <img src="<?= SITE_URL ?>/logo.svg" alt="" width="24" height="24" class="d-inline-block align-text-top">
            Microbe MVC
          </a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="<?= SITE_URL ?>/recipe">List of recipes</a>
              </li>
            </ul>
            <div class="d-flex">
              <a class="btn btn-success" href="<?= SITE_URL ?>/recipe/form">Add a recipe</a>
              <span class="navbar-text text-light ms-2"><?= SITE_VERSION ?></span>
            </div>
          </div>
        </div>
      </nav>
      <!-- START OF PAGE -->
      <div class="row">
        <div class="col-12 p-4">