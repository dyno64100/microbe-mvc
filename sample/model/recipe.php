<?php

/**
 * Microbe MVC: Sample usage - Recipe model
 *
 * @copyright 2022 Frederic Poeydomenge <dyno@aldabase.com>
 * @license https://opensource.org/licenses/MIT License MIT (#Expat)
 */

declare(strict_types=1);

class SampleModelRecipe extends MicrobeModel
{
    /** @var string Table name */
    protected string $table = 'recipe';

    /** @var string Table primary key */
    protected string $primaryKey = 'id';

    /** @var string[] Table fields names */
    protected array $allowedFields = ['name', 'type', 'difficulty', 'cost', 'servings'];

    /**
     * Find the latest recipe added to the site
     *
     * @return string[] Data of the last recipe added to the site
     */
    public function last() : array
    {
        return $this->db->query('SELECT * FROM recipe ORDER BY creation DESC LIMIT 1')->fetch();
    }

    /**
     * Provider for recipe type
     *
     * @return string[] Recipe type
     */
    public function providerType()
    {
        return ['Appetizer', 'Starter', 'Main course', 'Side dish', 'Dessert'];
    }

    /**
     * Provider for recipe difficulty
     *
     * @return string[] Recipe difficulty
     */
    public function providerDifficulty()
    {
        return ['Easy', 'Medium', 'Difficult'];
    }

    /**
     * Provider for recipe cost
     *
     * @return string[] Recipe cost
     */
    public function providerCost()
    {
        return ['Economical', 'Average', 'Expensive'];
    }
}
