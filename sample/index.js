/**
 * Microbe MVC: Custom Javascript
 *
 * @copyright 2022 Frederic Poeydomenge <dyno@aldabase.com>
 * @license https://opensource.org/licenses/MIT MIT Licence (#Expat)
 */

$(document).ready(function () {

    // Add an interactive confirmation before deletion
    $('.bi-trash-fill').each(function () {
        $(this).parent().click(function () {
            return confirm('Are you sure you want to delete this item ?');
        });
    });

});
