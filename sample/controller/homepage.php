<?php

/**
 * Microbe MVC: Sample usage - Homepage Controller
 *
 * @copyright 2022 Frederic Poeydomenge <dyno@aldabase.com>
 * @license https://opensource.org/licenses/MIT MIT Licence (#Expat)
 */

declare(strict_types=1);

class SampleControllerHomepage extends MicrobeController
{
    /**
     * Display of the homepage of the site
     */
    public function index() : void
    {
        $data = [
            'last' => (new SampleModelRecipe())->last(),
        ];
        echo $this->view->render('header'),
             $this->view->render('homepage/page', $data),
             $this->view->render('footer');
    }
}
