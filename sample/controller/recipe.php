<?php

/**
 * Microbe MVC: Sample usage - Recipe controller
 *
 * @copyright 2022 Frederic Poeydomenge <dyno@aldabase.com>
 * @license https://opensource.org/licenses/MIT License MIT (#Expat)
 */

declare(strict_types=1);

class SampleControllerRecipe extends MicrobeController
{
    /**
     * View the list of recipes
     */
    public function index() : void
    {
        $data = [
            'recipes' => (new SampleModelRecipe())->selectAll(),
        ];
        echo $this->view->render('header'),
             $this->view->render('recipe/list', $data),
             $this->view->render('footer');
    }

    /**
     * View the detail page of a recipe
     *
     * @param int $recipeId ID of the recipe to display
     */
    public function detail(int $recipeId) : void
    {
        $data = [
            'recipe' => (new SampleModelRecipe())->select($recipeId),
        ];
        echo $this->view->render('header'),
             $this->view->render('recipe/detail', $data),
             $this->view->render('footer');
    }

    /**
     * Management of the creation/update form
     *
     * @param null|int $recipeId ID of the recipe to display, or null if insert
     */
    public function form(?int $recipeId = null) : void
    {
        $model = new SampleModelRecipe();
        // If the form has been submitted, record the informations
        if (! empty($_POST)) {
            // Save data
            $model->save($_POST);
            // Passes a message to the calling page
            $message = empty($_POST['id'])
                ? 'The recipe has been added (ID=' . $model->lastInsertId() . ')'
                : 'The recipe has been updated';
            // Returns to the calling page
            $this->redirect(SITE_URL . '/recipe', $message);
        }
        // Otherwise, we load the record in case of update
        $data = [
            'recipe'             => $recipeId !== null ? $model->select($recipeId) : null,
            'providerType'       => $model->providerType(),
            'providerDifficulty' => $model->providerDifficulty(),
            'providerCost'       => $model->providerCost(),
        ];
        // And we display the form
        echo $this->view->render('header'),
             $this->view->render('recipe/form', $data),
             $this->view->render('footer');
    }

    /**
     * Delete a recipe
     *
     * @param int $recipeId ID of the recipe to delete
     */
    public function delete(int $recipeId) : void
    {
        (new SampleModelRecipe())->delete($recipeId);
        $this->redirect(SITE_URL . '/recipe', 'The recipe has been deleted');
    }
}
