#=========
# Makefile
#=========

ROOT = $(PWD)
UID = `id -u`
GID = `id -u`

###[ Coding Standard ]####################################################

# Base command
PHPCS_COMMAND = docker run --rm -it -v $(ROOT):/code -u "$(UID):$(GID)" aldabase/psr12cs
# Parameters to PHPCS/PHPCBF
PHPCS_PARAM = /code/microbe /code/sample \
	--exclude="PSR1.Files.SideEffects,Generic.PHP.ForbiddenFunctions,Squiz.Classes.ClassFileName,PSR1.Classes.ClassDeclaration,SlevomatCodingStandard.TypeHints.DeclareStrictTypes"

coding-standard:
	@$(PHPCS_COMMAND) /bin/phpcs -p ${PHPCS_PARAM} ; true

coding-standard-fix:
	@$(PHPCS_COMMAND) /bin/phpcbf -p ${PHPCS_PARAM} ; true

###[ Count source lines of code (SLOC) ]##################################

sloc-count:
ifneq (, $(shell which cloc))
	cloc --quiet --by-file $(ROOT)/microbe
	cloc --quiet --by-file --exclude-ext=svg,js,css $(ROOT)/sample
else ifneq (, $(shell which sloccount))
	sloccount $(ROOT)/microbe $(ROOT)/sample
else
	@echo "Commands not found: cloc or sloccount"
	@echo "Consider doing apt-get install cloc"
endif

###[ Initialisation Database ]############################################

init-db-mysql:
	mysql < $(ROOT)/create_mysql.sql

init-db-sqlite:
	sqlite3 $(ROOT)/sqlite.db < $(ROOT)/create_sqlite.sql
	chmod 0666 $(ROOT)/sqlite.db

###[ Static Analysis Tools for PHP ]######################################
# cf https://github.com/jakzal/phpqa

phplint:
	docker run --init -it --rm -v $(ROOT):/project -u "$(UID):$(GID)" jakzal/phpqa phplint /project/microbe /project/sample --no-cache
phpstan:
	docker run --init -it --rm -v $(ROOT):/project -u "$(UID):$(GID)" jakzal/phpqa phpstan analyse --level 6 /project/sample/index.php /project/microbe /project/sample
phpmd:
	docker run --init -it --rm -v $(ROOT):/project -u "$(UID):$(GID)" jakzal/phpqa phpmd /project/microbe,/project/sample  text codesize,design,naming,unusedcode
phpsec:
	docker run --init -it --rm -v $(ROOT):/project -u "$(UID):$(GID)" jakzal/phpqa phpcs --extensions=php,tpl --standard=Security /project/microbe /project/sample
phpinsight:
	docker run --init -it --rm -v $(ROOT):/project -u "$(UID):$(GID)" jakzal/phpqa phpinsights analyse /project/microbe /project/sample

###[ PHP Compatibility check ]############################################

phpcompat:
	docker run --init -it --rm -v $(ROOT)/microbe:/src1 -v $(ROOT)/sample:/src2 -u "$(UID):$(GID)" aldabase/phpcompat:latest -p --colors --extensions=php,tpl --runtime-set testVersion 7.4-8.1 /src1 /src2
