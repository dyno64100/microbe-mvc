-- ==============================================
-- Database initialization script (MySQL version)
-- ==============================================

SET NAMES utf8mb4;

DROP DATABASE IF EXISTS `microbe_mvc`;
CREATE DATABASE `microbe_mvc` CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_520_ci';

DROP USER IF EXISTS 'user_mvc'@'localhost';
CREATE USER 'user_mvc'@'localhost' IDENTIFIED BY 'pass_mvc';
GRANT ALL PRIVILEGES ON `microbe_mvc`.* TO 'user_mvc'@'localhost';
FLUSH PRIVILEGES;

USE `microbe_mvc`;

DROP TABLE IF EXISTS `recipe`;
CREATE TABLE `recipe` (
  `id`         int unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name`       varchar(100) NOT NULL,
  `type`       enum('Appetizer', 'Starter', 'Main course', 'Side dish', 'Dessert') NOT NULL,
  `difficulty` enum('Easy', 'Medium', 'Difficult') NOT NULL,
  `cost`       enum('Economical', 'Average', 'Expensive') NOT NULL,
  `servings`   int unsigned NOT NULL,
  `creation`   datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE='InnoDB';

INSERT INTO `recipe` (`id`, `name`, `type`, `difficulty`, `cost`, `servings`, `creation`) VALUES
(1, 'Paella', 'Main course', 'Easy', 'Average', 4, DATE_SUB(NOW(), INTERVAL 6 SECOND)),
(2, 'Chili con carne 💙', 'Main course', 'Medium', 'Average', 8, DATE_SUB(NOW(), INTERVAL 5 SECOND)),
(3, 'Patatas Bravas', 'Starter', 'Easy', 'Economical', 6, DATE_SUB(NOW(), INTERVAL 4 SECOND)),
(4, 'Fajitas', 'Appetizer', 'Easy', 'Average', 8, DATE_SUB(NOW(), INTERVAL 3 SECOND)),
(5, 'Cheesecake', 'Dessert', 'Easy', 'Economical', 6, DATE_SUB(NOW(), INTERVAL 2 SECOND)),
(6, 'Tiramisu 💚', 'Dessert', 'Easy', 'Economical', 4, DATE_SUB(NOW(), INTERVAL 1 SECOND));
