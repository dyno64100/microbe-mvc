# Microbe MVC

Microbe MVC is a **PHP microframework**, implementing MVC (Model View Controller) architecture, all in OOP (Object-Oriented Programming).

Its source code fits in roughly **80 lines of PHP code** !

Before starting to study its operation, prerequisite in PHP, MySQL and OOP are essential.

This microframework is an **educational project**, it is only suitable for **learning needs**. Its primary vocation is to present to beginner programmers a **voluntarily simple framework**, allowing them to read and understand how MVC and routing work together.

Read this in other languages: [English](README.md), [French](README.fr.md)

## Authors

- [Frederic Poeydomenge](https://gitlab.com/dyno64100)

## Description of the tree

```
microbe             Microbe MVC microframework
    bootstrap.php       Application bootstrap system
    controller.php      Base controller
    model.php           Base model
    view.php            Base view

sample              Sample application using the microframework
    controller/         Application controllers
    model/              Application models
    view/               Application views
    index.php           Entry point of the application
    .htaccess           Apache rewriting rules (cf routing)

apache2.conf        Apache configuration file
create_mysql.sql    Database initialization script (MySQL version)
create_sqlite.sql   Database initialization script (SQLite version)
```

## Download

```
git clone https://gitlab.com/dyno64100/microbe-mvc.git /var/www
```

## Installation

### 1. Creation of a local domain name

Under **Linux**, add in the **/etc/hosts** file:

    127.0.0.1 microbe-mvc.local

Under **Windows**, the hosts file can be found at the next location: `%SystemRoot%\system32\drivers\etc\hosts`

### 2. Creation of the Apache virtual host

An example of configuring a virtual host is in the **apache2.conf** file

It must be copied in the `sites-enabled` folder from Apache (_the procedure depends on your local installation LAMP/XAMPP/WAMP/EasyPHP/..._), then configure it according to your needs.

We configure **the local domain name**, previously defined, which will allow access to the site via a local address:

    ServerName microbe-mvc.local

as well as **the folder** in which the tree structure described previously was copied:

    DocumentRoot /var/www/microbe-mvc/sample/
    <Directory /var/www/microbe-mvc/sample/>

Under **Windows**, these 2 guidelines would rather look like something like:

    DocumentRoot "D:/www/microbe-mvc/sample/"
    <Directory "D:/www/microbe-mvc/sample/">

**Be careful!** You must be careful to make the virtual host point on the `sample` folder of Microbe MVC. This is the one in which the `index.php` entry point is stored.

### 3. Creation of the database

To store the test data, you can choose to use either a MySQL database or an SQLite database as a backend.

Depending on your choice of database, you will have to modify the PDO connector, in the `sample/index.php` script (`BDD_*` constants), and comment/uncomment the corresponding block of code (see "Define database-related constants")

#### 3.a MySQL

The database initialization script is in the **create_mysql.sql** file

It must be launched as a root/administrator in the MySQL instance installed on the machine

It performs the following operations:
- Creation of the database `microbe_mvc`
- Creation of the user `user_mvc` with the rights on the database
- Creation and filling of the test table

#### 3.b SQLite3

Using the DB Browser for SQLite, you must first create a new database at the root of the site, named `sqlite.db`.

Then just import the **create_sqlite.sql** file (File menu > Import) to create and fill the test table

### 4. Browse the website

Point your browser on the local domain name (http://microbe-mvc.local/)

The home page should now appear !

And now, it's your turn !!!

## Alternative features

Other features are available on the following GIT branches:

- [feature/namespace](https://gitlab.com/dyno64100/microbe-mvc/-/tree/feature/namespace) : example of using PHP namespaces
- [feature/phpunit](https://gitlab.com/dyno64100/microbe-mvc/-/tree/feature/phpunit) : example of performing unit testing

To update the code, in order to test a branch above, do a:

```
git checkout feature/<Feature>
```

## Appendix

### URL routing

The routing is responsible for the correspondence of the incoming HTTP requests (_eg. the address entered in the browser_) and the distribution of these requests to the executable termination points of the application (_eg. the method of the controller to call_)

In Microbe MVC, it is not necessary to define the route manually, the routing is automatic, and transports the HTTP request according to the different components of the URL: eg. for the request http://nomedomaine/recipe/recipe/detail/12, the system will call the controller **Recipe**, method **detail()**, by passing it as a parameter the value **12**

To do this, we use two distinct mechanisms:

- The **Apache mod_rewrite module**, which allows the URL to correspond to the file system. The detailed instructions in the configuration file `Sample/.htaccess` indicate to Apache that all requests not physically corresponding to a file are routed to the file` sample/index.php`.
- The **classes auto-loader (autoload)**, defined in the `microbe/bootstrap.php` script which is loaded when starting the page, and which allows to map the name of the requested class with a physical file in the tree.

## Licence

This project is distributed under Expat Licence (#expat) also known as the « MIT Licence »
[See the full text of the MIT Licence (#Expat)](LICENCE.md)

The "Microbe" logo is distributed under Attribution 3.0 Unported (CC BY 3.0) Licence
[Download the icon on Iconfinder](https://www.iconfinder.com/icons/6009592)
