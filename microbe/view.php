<?php

/**
 * Microbe MVC: Base view
 *
 * @copyright 2022 Frederic Poeydomenge <dyno@aldabase.com>
 * @license https://opensource.org/licenses/MIT MIT Licence (#Expat)
 */

declare(strict_types=1);

class MicrobeView
{
    /**
     * Renders the view
     *
     * @param string  $viewName View name
     * @param mixed[] $data     Data passed to the view
     * @return string HTML code of the view
     */
    public function render(string $viewName, array $data = []) : string
    {
        // Extract data into local variables
        extract($data);
        // Then return the HTML code of the view
        ob_start();
        require_once MICROBE_VIEW_DIR . $viewName . '.tpl';
        return (string) ob_get_clean();
    }
}
