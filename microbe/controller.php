<?php

/**
 * Microbe MVC: Base controller
 *
 * @copyright 2022 Frederic Poeydomenge <dyno@aldabase.com>
 * @license https://opensource.org/licenses/MIT MIT Licence (#Expat)
 */

declare(strict_types=1);

class MicrobeController
{
    // View object instance
    protected MicrobeView $view;

    /**
     * Constructor - Instantiate a View object
     */
    public function __construct()
    {
        $this->view = new MicrobeView();
    }

    /**
     * Helper: Redirection to the calling page,
     * optionally by passing a message through the session
     *
     * @param string      $url     URL to which the browser is redirected
     * @param null|string $message Message to record in session
     */
    public function redirect(string $url, ?string $message = null) : void
    {
        $_SESSION['message'] = $message;
        header('Location:' . $url);
    }
}
