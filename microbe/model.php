<?php

/**
 * Microbe MVC: Base model
 *
 * @copyright 2022 Frederic Poeydomenge <dyno@aldabase.com>
 * @license https://opensource.org/licenses/MIT MIT Licence (#Expat)
 */

declare(strict_types=1);

class MicrobeModel
{
    /** @var string Table name */
    protected string $table = 'table';

    /** @var string Table primary key */
    protected string $primaryKey = 'id';

    /** @var string[] Table fields names */
    protected array $allowedFields = [];

    /** @var PDO Database object instance */
    protected PDO $db;

    /**
     * Constructor: Initialize database connection
     */
    public function __construct()
    {
        $this->db = new PDO(BDD_DSN, BDD_USER, BDD_PASSWORD);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * [CRUD] Retrieve a list of records
     *
     * @return mixed[] Array containing all the records of the table
     */
    public function selectAll() : array
    {
        return $this->db->query('SELECT * FROM ' . $this->table)->fetchAll();
    }

    /**
     * [CRUD] Retrieve a single record
     *
     * @param int $id Record ID
     * @return string[] Array containing the details of the record
     */
    public function select(int $id) : array
    {
        return $this->db->query('SELECT * FROM ' . $this->table . ' WHERE `' . $this->primaryKey . '`=' . $id)->fetch();
    }

    /**
     * [CRUD] Save a record (Insert/Update)
     *
     * @param string[] $data Data to be saved in BDD
     * @return bool True if successful or False if an error occurred
     */
    public function save(array $data = []) : bool
    {
        // Constructs the basic query based on named parameters (:param)
        $keys = [];
        foreach ($data as $key => $val) {
            if ($key === $this->primaryKey || in_array($key, $this->allowedFields)) {
                $keys[] = $key;
            }
        }

        // If the primary key is empty, set it to NULL (= autoincrement)
        $data[$this->primaryKey] = empty($data[$this->primaryKey]) ? null : $data[$this->primaryKey];

        // Finalize the query, based on query type:
        // Update if primary key is set, Insert otherwise
        $query = (empty($data[$this->primaryKey]) ? 'INSERT INTO ' : 'REPLACE INTO ') . $this->table
                    . ' (' . implode(',', $keys) . ') VALUES (:' . implode(',:', $keys) . ')';

        // Run SQL query
        return $this->db->prepare($query)->execute($data);
    }

    /**
     * [PDO] ID of the last inserted primary key
     *
     * @return int Primary key ID
     */
    public function lastInsertId() : int
    {
        return (int) $this->db->lastInsertId();
    }

    /**
     * [CRUD] Delete a record
     *
     * @param int $id Record ID
     * @return bool True if successful or False if an error occurred
     */
    public function delete(int $id) : bool
    {
        return $this->db->query('DELETE FROM ' . $this->table . ' WHERE `' . $this->primaryKey . '`=' . $id)->fetch();
    }
}
