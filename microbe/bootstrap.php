<?php

/**
 * Microbe MVC: Application bootstrapping system
 *
 * @copyright 2022 Frederic Poeydomenge <dyno@aldabase.com>
 * @license https://opensource.org/licenses/MIT MIT Licence (#Expat)
 */

try {
    // Implementation of the class auto-loading mechanism
    spl_autoload_register(function (string $class) : void {
        // Eg. if the SampleControllerHomepage class is not currently defined,
        // we will try to load it from the following file : ../sample/controller/homepage.php
        require_once __DIR__ . '/..' . preg_replace_callback('#([[:upper:]])#', function ($match) : string {
            return '/' . strtolower($match[1]);
        }, $class) . '.php';
    });

    // Starts a new session or resumes the current one
    session_start();

    // Splits the components passed in the URL into segments
    // Eg. if you access the URL : "/recipe/detail/5",
    //     the $route variable will contain : ['recipe', 'detail', '5']
    $route = empty($_SERVER['REDIRECT_URL']) ? [] : explode('/', trim($_SERVER['REDIRECT_URL'], '/'));

    // Determines the controller to call (this is the first segment of the route)
    // Eg. if you access the URL : "/recipe/detail/5",
    //     the $controller variable will contain : "SampleControllerRecipe"
    $controller = MICROBE_PREFIX . ucfirst($route[0] ?? MICROBE_CONTROL);

    // Calls the requested method (this is the second segment of the route)
    // passing it the rest of the route as arguments
    call_user_func_array([new $controller(), $route[1] ?? 'index'], array_slice($route, 2));

// Capture of a potential exception, and display of the corresponding message
} catch (Exception $e) {
    trigger_error($e->getMessage(), E_USER_ERROR);
}
