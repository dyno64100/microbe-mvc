-- ===============================================
-- Database initialization script (SQLite version)
-- ===============================================

DROP TABLE IF EXISTS `recipe`;
CREATE TABLE `recipe` (
    `id`         INTEGER PRIMARY KEY AUTOINCREMENT,
    `name`       TEXT CHECK(LENGTH(name) <= 100) NOT NULL,
    `type`       TEXT CHECK(type IN ('Appetizer', 'Starter', 'Main course', 'Side dish', 'Dessert')) NOT NULL,
    `difficulty` TEXT CHECK(difficulty IN ('Easy', 'Medium', 'Difficult')) NOT NULL,
    `cost`       TEXT CHECK(cost IN ('Economical', 'Average', 'Expensive')) NOT NULL,
    `servings`   INTEGER NOT NULL,
    `creation`   DATETIME DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO `recipe` (`id`, `name`, `type`, `difficulty`, `cost`, `servings`, `creation`) VALUES
(1, 'Cassoulet', 'Main course', 'Easy', 'Average', 4, datetime('now', '-6 second')),
(2, 'Spaghetti bolognese 💙', 'Main course', 'Medium', 'Average', 8, datetime('now', '-5 second')),
(3, 'Moussaka', 'Starter', 'Easy', 'Economical', 6, datetime('now', '-4 second')),
(4, 'Enchiladas', 'Appetizer', 'Easy', 'Average', 8, datetime('now', '-3 second')),
(5, 'Baklava', 'Dessert', 'Easy', 'Economical', 6, datetime('now', '-2 second')),
(6, 'Creme Brulee 💚', 'Dessert', 'Easy', 'Economical', 4, datetime('now', '-1 second'));
